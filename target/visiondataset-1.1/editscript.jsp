<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<tiles:insertDefinition name="templateVisionDataset">
    <tiles:putAttribute name="content">

        <h1>EDIT SCRIPT</h1>

            <c:if test="${userHasCreatePermission}">
                <s:form class="scriptBox" action="modifyscript" >
                    <s:hidden class="scriptBox" name="scriptId" value="%{scriptId}"/>
                    <s:hidden class="scriptBox" name="imageId" value="%{imageId}"/>
                    <s:textarea class="scriptBox" name="resultname" label="Result name:" rows="1" cols="40"/>
                    <s:textarea class="scriptBox" name="scriptcontent" label="Script" value="%{script}" rows="50" cols="100"/>
                    <s:radio name="scriptoption" list="{'Save script','Save and run', 'Delete script'}"/>
                <s:submit />
                </s:form>
             </c:if>

    </tiles:putAttribute>
</tiles:insertDefinition>


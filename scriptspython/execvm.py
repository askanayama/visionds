import sys
from PIL import Image
import smtplib
import getpass
from socket import *
import time

if __name__ == "__main__":
    script = sys.argv[1]
    imageId = sys.argv[2]
    name = sys.argv[3]
    mail = sys.argv[4]
    att = sys.argv[5]
    sub = sys.argv[6]
    resultId = sys.argv[7]
    f = open('/tmp/time'+imageId, 'w')
    f.write(str(time.time()) + '\n')
    #image = Image.open("/var/local/visionDataset/images/"+imageId)
    principal_mod = __import__(script, "principal")
    if str(att)== '1':
        result = principal_mod.principal("/var/local/visionDataset/images/"+imageId, '/var/local/visionDataset/attachments/'+name)
    if str(sub) == '1':
        result = principal_mod.principal("/var/local/visionDataset/images/"+imageId, '/var/local/visionDataset/images/'+resultId)
    #result.save('/var/local/visionDataset/attachments/'+name)
    if mail != 'nop':
        server = 'smtp.gmail.com'
        port = 587
 
        sender = 'vdataset@gmail.com'
        password = 's3nh4VDSm4il'
        recipient = mail
        subject = '[VisionDataset] Processamento de imagem terminado'
        body = 'Processamento da imagem ' + imageId + ' terminado. Resultado encontra-se em ' + name + '.'
     
        body = "" + body + ""
     
        headers = ["From: " + sender,
               "Subject: " + subject,
               "To: " + recipient,
               "MIME-Version: 1.0",
               "Content-Type: text/html"]
        headers = "\r\n".join(headers)
     
        session = smtplib.SMTP(server, port)
     
        session.ehlo()
        session.starttls()
        session.ehlo
        session.login(sender, password)
     
        session.sendmail(sender, recipient, headers + "\r\n\r\n" + body)
        session.quit()
    f.write(str(time.time()) + '\n')
    f.close()

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<tiles:insertDefinition name="templateVisionDataset">
	<tiles:putAttribute name="content">

		<h1>RESULT</h1>
        
        <c:if test="${action == 1}">
            <p>Script saved!</p>
        </c:if>

        <c:if test="${action == 2}">
            <p><s:property value="output" /></p>
        </c:if>

        <c:if test="${action == 3}">
            <p>Script deleted!</p>
        </c:if>

	</tiles:putAttribute>
</tiles:insertDefinition>

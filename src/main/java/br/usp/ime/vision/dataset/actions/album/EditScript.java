package br.usp.ime.vision.dataset.actions.album;

import org.apache.commons.lang.xwork.StringUtils;

import br.usp.ime.vision.dataset.entities.ImageScript;
import br.usp.ime.vision.dataset.util.Messages;

import br.usp.ime.vision.dataset.dao.DAOFactory;
import br.usp.ime.vision.dataset.entities.Album;
import br.usp.ime.vision.dataset.entities.Image;
import java.io.*;
import java.util.Scanner;

/**
 * Action for script edition.
 * 
 * @author André Kanayama
 */
public class EditScript extends AlbumAction {

    private static final long serialVersionUID = 1L;

    private int scriptId;

    public String script;
   public int imageId; 

 @Override
    public String execute() throws Exception {

        if (getScript() == null) {
            return INVALID_REQUEST;
        }

        if (!isUserHasViewPermission()) {
            return UNAUTHORIZED_ACTION;
        }
        System.out.println(imageId);        
        return "SUCCESS";

    }

    public String getScript() {
        try{
            String s = new Scanner( new File("/var/local/visionDataset/scripts/" + scriptId + ".py"), "UTF-8" ).useDelimiter("\\A").next();
            return s;
        }
        catch(FileNotFoundException e){
            return null;
        }
    }

    public int getScriptId() {
        return scriptId;
    }

    public void setScriptId(final int scriptId) {
        this.scriptId = scriptId;
    }


}

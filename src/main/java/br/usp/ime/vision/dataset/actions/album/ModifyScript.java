package br.usp.ime.vision.dataset.actions.album;

import org.apache.commons.lang.xwork.StringUtils;

import br.usp.ime.vision.dataset.entities.ImageScript;
import br.usp.ime.vision.dataset.util.Messages;
import java.util.List;

import br.usp.ime.vision.dataset.dao.DAOFactory;
import br.usp.ime.vision.dataset.entities.Album;
import br.usp.ime.vision.dataset.entities.Image;
import br.usp.ime.vision.dataset.entities.User;
import java.io.*;
import java.util.Scanner;
import org.apache.commons.io.FilenameUtils;

/**
 * Action for script modification.
 * 
 * @author André Kanayama
 */
public class ModifyScript extends AlbumAction {

    private static final long serialVersionUID = 1L;

    public int scriptId;
    
    public String resultname;
    public String scriptcontent;
    public String scriptoption;
    public int imageId;
    public String output = "";
    public String scriptmail;
    public int action = 0;
    public String scriptattachment;
    public String scriptsubalbum;

 @Override
    public String execute() throws Exception {
        System.out.println(scriptmail);
        System.out.println(scriptoption);
        //final User user = DAOFactory.getUserDao().getUserByUsername(getUserName());
        final User user = getLoggedUser();
        int att = 0;
        int sub = 0;
        System.out.println(scriptattachment);
        System.out.println(scriptsubalbum);
        int resultId = -1;

	if (scriptattachment.equals("true")){
		att = 1;
	}
	if (scriptsubalbum.equals("true")) sub = 1;
        if (scriptoption.equals("Save script")) {
            action = 1;
            BufferedWriter writer = null;
            //File sc = new File("/var/local/visionDataset/scripts/"+scriptId);
            //sc.delete();
            try{
                writer = new BufferedWriter( new FileWriter("/var/local/visionDataset/scripts/" + scriptId + ".py"));
                writer.write(scriptcontent);
                writer.close();
            }
            catch ( IOException e){
            }

        }
/**************************************************************************************************************************************/
        else if (scriptoption.equals("Save and run")) {
            action = 2;
            BufferedWriter writer = null;
            //File sc = new File("/var/local/visionDataset/scripts/"+scriptId);
            //sc.delete();
            try{
                writer = new BufferedWriter( new FileWriter("/var/local/visionDataset/scripts/" + scriptId + ".py"));
                writer.write(scriptcontent);
                writer.close();
            }
            catch ( IOException e){
            }
	    if(sub == 1){
		String ext = FilenameUtils.getExtension(resultname);
		String type;
		int albumid;
		if(ext.equals("jpg") && ext.equals("jpeg")){
			type = "image/jpeg";
		}
		else if(ext.equals("png") && ext.equals("jpeg")){
			type = "image/png";
		}
		else{
			type = "image/" + ext;
		}
		albumid = DAOFactory.getAlbumDao().getResultAlbumFromImage(imageId);
		resultId = DAOFactory.getAlbumDao().addImageToAlbum(albumid, getLoggedUser().getId(), type);
		
	}
            try
            {
                Runtime r = Runtime.getRuntime();
                System.out.println("python /var/local/visionDataset/scripts/executor.py "+ scriptId + " " + imageId + " " + resultname + user.getEmail());
                if(scriptmail.equals("true")){
                    Process p = r.exec("python /var/local/visionDataset/scripts/executor.py "+ scriptId + " " + imageId + " " + resultname + " " + user.getEmail() + " " + att + " " + sub + " " + resultId);
                }
                else{
        PrintWriter writerb = new PrintWriter("/tmp/coisa", "UTF-8");
        writerb.println("python /var/local/visionDataset/scripts/executor.py "+ scriptId + " " + imageId + " " + resultname + " " + "nop" + " " + att + " " + sub + " " + resultId);
        writerb.close();
                    
                    Process p = r.exec("python /var/local/visionDataset/scripts/executor.py "+ scriptId + " " + imageId + " " + resultname + " " + "nop" + " " + att + " " + sub + " " + resultId);
                }
                //BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
                //p.waitFor();
                //while (br.ready())
                //    output+=br.readLine();
		if(att == 1){
                	DAOFactory.getAttachmentDAO().addImageAttachment(imageId, resultname);
		}
                output = "Success"; 
            }
            catch (Exception e)
            {
            String cause = e.getMessage();
            if (cause.equals("python: not found"))
                output = ("No python interpreter found.");
            else
                System.out.println(cause);
                output = cause;
            }

        }
/**********************************************************************************************************************************************/
        else if (scriptoption.equals("Save and run for album")) {
            action = 2;
            BufferedWriter writer = null;
            Album album;
            int albumId;
            album = DAOFactory.getAlbumDao().getAlbumForImage(imageId);
            //albumId = album.getId();
            List<Image> images = album.getImages();
            //File sc = new File("/var/local/visionDataset/scripts/"+scriptId);
            //sc.delete();
            for (Image im : images){
                imageId = im.getId();
                try{
                    writer = new BufferedWriter( new FileWriter("/var/local/visionDataset/scripts/" + scriptId + ".py"));
                    writer.write(scriptcontent);
                    writer.close();
                }
                catch ( IOException e){
                }
                if(sub == 1){
                    String ext = FilenameUtils.getExtension(resultname);
                    String type;
                    int albumid;
                    if(ext.equals("jpg") && ext.equals("jpeg")){
                            type = "image/jpeg";
                    }
                    else if(ext.equals("png") && ext.equals("jpeg")){
                            type = "image/png";
                    }
                    else{
                            type = "image/" + ext;
                    }
                    albumid = DAOFactory.getAlbumDao().getResultAlbumFromImage(imageId);
                    resultId = DAOFactory.getAlbumDao().addImageToAlbum(albumid, getLoggedUser().getId(), type);
                }
                try
                {
                    Runtime r = Runtime.getRuntime();
                    System.out.println("python /var/local/visionDataset/scripts/executor.py "+ scriptId + " " + imageId + " " + imageId + "_" + resultname + user.getEmail());
                    if(scriptmail.equals("true")){
                        Process p = r.exec("python /var/local/visionDataset/scripts/executor.py "+ scriptId + " " + imageId + " " + imageId + "_" + resultname + " " + user.getEmail() + " " + att + " " + sub + " " + resultId);
                    }
                    else{
                        Process p = r.exec("python /var/local/visionDataset/scripts/executor.py "+ scriptId + " " + imageId + " " + imageId + "_" + resultname + " " + "nop" + " " + att + " " + sub + " " + resultId);
                    }
                    //BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
                    //p.waitFor();
                    //while (br.ready())
                    //    output+=br.readLine();
                    if(att == 1){
                        DAOFactory.getAttachmentDAO().addImageAttachment(imageId,imageId + "_" + resultname);
                    }

                    output = "Success"; 
                }
                catch (Exception e)
                {
                String cause = e.getMessage();
                if (cause.equals("python: not found"))
                    output = ("No python interpreter found.");
                else
                    System.out.println(cause);
                    output = cause;
                }
            }
        }

        else{
            action = 3;
            boolean result = DAOFactory.getScriptDAO().deleteScript(scriptId);
            File sc = new File("/var/local/visionDataset/scripts/"+scriptId + ".py");
            sc.delete();
        }
        return "SUCCESS";

    }

    public String getScript() {
        try{
            String s = new Scanner( new File("/var/local/visionDataset/scripts/" + scriptId + ".py"), "UTF-8" ).useDelimiter("\\A").next();
            return s;
        }
        catch(FileNotFoundException e){
            return null;
        }
    }

    public int getScriptId() {
        return scriptId;
    }

    public int getImageId() {
        return imageId;
    }

    public void setScriptId(final int scriptId) {
        this.scriptId = scriptId;
    }


}

package br.usp.ime.vision.dataset.actions.album;

import br.usp.ime.vision.dataset.entities.Album;
import br.usp.ime.vision.dataset.entities.Image;
import br.usp.ime.vision.dataset.util.ImageUtils;
import java.util.List;

/**
 * Action for {@link Album} visualization.
 * 
 * @author Bruno Klava
 */
public class AlbumDetail extends AlbumAction {

    private static final long serialVersionUID = 1L;

    @Override
    public String execute() throws Exception {

	album = getAlbum();
	List<Image> images = album.getImages();
	for (final Image image : images){
		try{
		ImageUtils.createThumbnail(image.getId());
		}
		catch(final Exception e){
			System.out.println("erro");
		}
	}

        if (getAlbum() == null) {
            return INVALID_REQUEST;
        }

        if (!isUserHasViewPermission()) {
            return UNAUTHORIZED_ACTION;
        }

        return SUCCESS;

    }
}

package br.usp.ime.vision.dataset.actions.album;

import org.apache.commons.lang.xwork.StringUtils;

import br.usp.ime.vision.dataset.entities.ImageScript;
import br.usp.ime.vision.dataset.util.Messages;

import br.usp.ime.vision.dataset.dao.DAOFactory;
import br.usp.ime.vision.dataset.entities.Album;
import br.usp.ime.vision.dataset.entities.Image;
import java.io.*;

/**
 * Action for script running and visualization.
 * 
 * @author André Kanayama
 */
public class ShowScript extends AlbumAction {

    private static final long serialVersionUID = 1L;

    private int scriptId;

    
    public String script;
    public String scriptName;
    public int imageId;
    public String message = "";

    public String execute() {
    
        //setScript(getScript());
        scriptId = DAOFactory.getScriptDAO().addImageScript(getImageId(), scriptName);
        if (scriptId != 0) {
            //addActionMessage(Messages.getMessage("success.add.image.script", script));
        }

        else {
        //addActionError(Messages.getMessage("error.add.image.script"));
        return INPUT;
        }

        BufferedWriter writer = null;
        try{
            writer = new BufferedWriter( new FileWriter("/var/local/visionDataset/scripts/" + scriptId + ".py"));
            writer.write(script);

        }
        catch ( IOException e){
        }
        finally{
            try{
                if ( writer != null)
                writer.close( );
            }
            catch ( IOException e){
            }
        }
        /*try
        {
            Runtime r = Runtime.getRuntime();
            Process p = r.exec("python /var/local/visionDataset/scripts/" + scriptId);
            BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
            p.waitFor();
            while (br.ready())
                message+=br.readLine();

        }
        catch (Exception e)
        {
		String cause = e.getMessage();
		if (cause.equals("python: not found"))
			System.out.println("No python interpreter found.");
        }*/
        setScript("Script salvo com sucesso");
        return "SUCCESS";
    }

    public String getScript() {
        return script;
    }

    public void setScript(String script) {
        this.script = script;
    }

     public int getImageId() {
        return imageId;
    }
    
    /*public String execute() {
        setScript(getScript());
        return "SUCCESS";
    }

    public String getScript() {
        return script;
    }

    public void setScript(String script) {
        this.script = script;
    }*/

}

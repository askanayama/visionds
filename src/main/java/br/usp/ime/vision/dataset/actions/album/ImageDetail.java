package br.usp.ime.vision.dataset.actions.album;

import br.usp.ime.vision.dataset.dao.DAOFactory;
import br.usp.ime.vision.dataset.entities.Album;
import br.usp.ime.vision.dataset.entities.Image;
import java.io.*;
import java.util.Scanner;

/**
 * Action for {@link Image} visualization.
 * 
 * @author Bruno Klava
 */
public class ImageDetail extends AlbumAction {

    private static final long serialVersionUID = 1L;

    private int imageId;

    private Image image;

    public String content;

    @Override
    public String execute() throws Exception {

        if (getImage() == null || getContent() == null) {
            return INVALID_REQUEST;
        }

        if (!isUserHasViewPermission()) {
            return UNAUTHORIZED_ACTION;
        }

        return SUCCESS;

    }

    @Override
    public Album getAlbum() {
        if (album == null) {
            album = DAOFactory.getAlbumDao().getAlbum(getImage().getAlbumId());
        }
        return album;
    }

    @Override
    public int getAlbumId() {
        return getImage().getAlbumId();
    }

    public Image getImage() {
        if (image == null) {
            image = DAOFactory.getAlbumDao().getImage(getImageId());
        }
        return image;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(final int imageId) {
        this.imageId = imageId;
    }

     public String getContent() {
        try{
            String s = new Scanner( new File("/var/local/visionDataset/scripts/model.py"), "UTF-8" ).useDelimiter("\\A").next();
            return s;
        }
        catch(FileNotFoundException e){
            return null;
        }
    }


}
